package com.readytogrok.code;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Random;

import static com.readytogrok.code.AppModule.MAX_SIZE;
import static com.readytogrok.code.AppModule.MIN_SIZE;

/**
 * Created by Frank on 6/28/2017.
 */
public class RandomSize {
    private int size;

    @Inject
    public RandomSize(
            @Named(MIN_SIZE) int minBits,
            @Named(MAX_SIZE) int maxBits) {
        Random random = new Random();
        size = random.nextInt(maxBits - minBits) + minBits;
    }

    int getSize() {
        return size;
    }
}
