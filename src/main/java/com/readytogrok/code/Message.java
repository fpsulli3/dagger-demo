package com.readytogrok.code;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Frank on 6/29/2017.
 */
@MessageScope
public class Message {
    public final static String NAME_MESSAGE_ID = "com.readytogrok.code.name.MESSAGE_ID";

    String msg;

    @Inject
    public Message(
            RandomString randomString,
            @Named(NAME_MESSAGE_ID) String messageId) {
        final String NEW_LINE = System.getProperty("line.separator");

        msg  = "----------" + NEW_LINE;
        msg += "id: " + messageId + NEW_LINE;
        msg += "----------" + NEW_LINE;
        msg += "body: " + randomString + NEW_LINE;
        msg += "----------" + NEW_LINE;
    }

    @Override
    public String toString() {
        return msg;
    }
}
