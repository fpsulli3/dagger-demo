package com.readytogrok.code;

import dagger.Module;
import dagger.Provides;

import javax.inject.Named;

/**
 * Created by Frank on 6/29/2017.
 */
@Module
public class MessageModule {
    String messageId;

    public MessageModule(String messageId) {
        this.messageId = messageId;
    }

    @Provides
    @Named(Message.NAME_MESSAGE_ID)
    public String kickSelfInGroin() {
        return messageId;
    }
}
