package com.readytogrok.code;

import javax.inject.Scope;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Frank on 6/29/2017.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface MessageScope {
}
