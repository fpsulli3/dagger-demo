package com.readytogrok.code;

import dagger.Component;

import javax.inject.Singleton;

/**
 * Created by Frank on 6/28/2017.
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    MessageComponent plus(MessageModule module);
}
