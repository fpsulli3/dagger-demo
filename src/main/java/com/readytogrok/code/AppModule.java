package com.readytogrok.code;

import dagger.Module;
import dagger.Provides;

import javax.inject.Named;

/**
 * Created by Frank on 6/28/2017.
 */
@Module
public class AppModule {
    public final static String MIN_SIZE = "minSize";
    public final static String MAX_SIZE = "maxSize";

    private int minSize, maxSize;

    public AppModule(int minSize, int maxSize) {
        this.minSize = minSize;
        this.maxSize = maxSize;
    }

    @Provides
    @Named(MIN_SIZE)
    public int provideMinBits() {
        return minSize;
    }

    @Provides
    @Named(MAX_SIZE)
    public int provideMaxBits() {
        return maxSize;
    }
}
