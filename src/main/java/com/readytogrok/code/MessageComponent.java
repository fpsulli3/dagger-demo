package com.readytogrok.code;

import dagger.Subcomponent;

/**
 * Created by Frank on 6/29/2017.
 */
@MessageScope
@Subcomponent(modules = MessageModule.class)
public interface MessageComponent {
    Message getMessage();
}
