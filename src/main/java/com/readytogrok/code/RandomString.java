package com.readytogrok.code;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigInteger;
import java.util.Random;

/**
 * Created by Frank on 6/28/2017.
 */
@Singleton
public class RandomString {
    private String str;

    @Inject
    public RandomString(RandomSize randomSize) {
        Random random = new Random();
        str = new BigInteger(randomSize.getSize(), random).toString(32);
    }

    @Override
    public String toString() {
        return str;
    }
}
