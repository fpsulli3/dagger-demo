package com.readytogrok.code;

/**
 * Created by Frank on 6/28/2017.
 */
public class Main {

    public static void main(String[] args){
        Main app = new Main();
        app.run();
    }

    private void run() {
        AppComponent component = DaggerAppComponent
                                    .builder()
                                    .appModule(new AppModule(32, 64))
                                    .build();

        printMessage(component, "FOO");
        printMessage(component, "BAR");
    }

    private void printMessage(AppComponent component, String messageId) {
        MessageComponent messageComponent = component.plus(new MessageModule(messageId));
        System.out.printf("%s", messageComponent.getMessage());
    }
}
